package com.example.test_flutter_to_android_app2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Map;

import io.flutter.Log;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.FlutterEngineCache;
import io.flutter.embedding.engine.dart.DartExecutor;
import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends AppCompatActivity {

    private static final String CHANNEL = "express";
    final  String userSessionId = "0352feb3-1581-4d6c-b859-5c7c0a88485c905031020";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button button = findViewById(R.id.button_id);
        /* startActivity(FlutterActivity.createDefaultIntent(this));
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(
                                FlutterActivity
                                        .withNewEngine()
                                        .initialRoute("/test_flutter_merge")
                                        .build(MainActivity.this)
                        );
                    }
                });  */
    }

    public void launchFlutter(View view) {
        FlutterEngine flutterEngine = new FlutterEngine(this);
        flutterEngine.getDartExecutor().executeDartEntrypoint(
                DartExecutor.DartEntrypoint.createDefault()
        );

        MethodChannel mc = new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(),CHANNEL);
        mc.setMethodCallHandler((methodCall, result) ->
                {
                    if(methodCall.method.equals("express")) {
                        // result.success(userSessionId);
                        Log.i("new method came!",methodCall.method);
                        Intent intent = new Intent(this, CaseActivity.class);
                        startActivity(intent);
                    } else {
                        Log.i("new method came",methodCall.method);
                    }

                }
        );

        Map<String,String> map = Map.of(
                "accessToken" , "eyJraWQiOiJ6MlhyY2xFUlhVbkY4T01ndmcraWZvWkw5dCt2RStVUHptZ1d",
                "action", "landing",
                "env","ALPHA",
                "idToken", "eyJrabWUiO1BZyiTKAE9taTcvAPyzjf0AWpDbDeE3BJsquQ",
                "language", "TH",
                "userId","rbh-guestad7a19c1-be92-458f-ac27-0f2368512cf4",
                "userType", "GUEST"
        );

        mc.invokeMethod("express",map);


        FlutterEngineCache
                .getInstance()
                .put("myUserId", flutterEngine);
        flutterEngine.getNavigationChannel().setInitialRoute("/");

        startActivity(
                FlutterActivity
                        .withCachedEngine("myUserId")
                        .build(this)
        );
    }
}