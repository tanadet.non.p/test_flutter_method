package com.example.test_flutter_to_android_app2;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Map;

import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.dart.DartExecutor;
import io.flutter.plugin.common.MethodChannel;

public class CaseActivity extends AppCompatActivity {

    private static final String CHANNEL = "express";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_case);






    }

    public void backToFlutter(View view){
        FlutterEngine flutterEngine = new FlutterEngine(this);
        flutterEngine.getDartExecutor().executeDartEntrypoint(
                DartExecutor.DartEntrypoint.createDefault()
        );
        MethodChannel mc = new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(),CHANNEL);

        Map<String,String> map = Map.of(
                "accessToken" , "eyJraWQiOiJ6MlhyY2xFUlhVbkY4T01ndmcraWZvWkw5dCt2RStVUHptZ1d",
                "action", "expressPayment",
                "env","ALPHA",
                "idToken", "eyJrabWUiO1BZyiTKAE9taTcvAPyzjf0AWpDbDeE3BJsquQ",
                "language", "TH",
                "userId","rbh-guestad7a19c1-be92-458f-ac27-0f2368512cf4",
                "userType", "GUEST"
        );

         mc.invokeMethod("express",map);
         finish();
         // result.success(userSessionId);
    }
}
